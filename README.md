# preload-rename-thumb

preload-rename-thumb is a `LD_PRELOAD` library that hooks to `rename` syscalls to:
- let the file/folder be renamed as intended
- if there were [XDG thumbnails](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html) associated to files/folders, update them to the new location

The goal is to avoid rebuilding a lot of thumbnails when a folder is moved.

In addition, it hooks `unlink` syscalls to remove spurious thumbnails.

## How to use

### Build

Requires gcc and ImageMagick

    make

### Example usage

    LD_PRELOAD=preload-rename-thumb.so mv folder_containing_videos new_name

Since it hooks `rename` and `renameat2`, it can probably be used with graphical file managers like Thunar or spacefm.

## Caveats

- When moving a file/folder to another filesystem, `rename` is not used, but a copy-then-delete, which cannot be handled the same
  - see [mv-with-thumb](https://gitlab.com/hydrargyrum/attic/-/tree/master/mv-with-thumb) for that
- Moving a folder requires listing the whole subtree, because a single `rename` can update many files' thumbnails
- Simpler libpng should be used instead of heavyweight ImageMagick

## Dependencies

- ImageMagick

Unit tests require:
- Python 3.7
- pytest
- [vignette](https://pypi.org/project/vignette/)

## License

[WTFPLv2](https://wtfpl.net)
