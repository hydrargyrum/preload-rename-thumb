#include "walk.h"

void rename_thumbs(const char *oldpath, const char *newpath);

void rename_thumbs_dir(const char *newpath, const struct mylist *treehashes);

void unlink_thumbs(const char *path);
