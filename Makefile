
all: preload-rename-thumb.so

clean:
	-rm -f preload-rename-thumb.so *.o

CFLAGS = $(shell pkg-config --cflags MagickWand) -Wall -D_GNU_SOURCE -D_DEFAULT_SOURCE -fPIC
LDFLAGS = $(shell pkg-config --libs MagickWand) -ldl -shared

preload-rename-thumb.so: png.o preload-rename-thumb.o md5.o real-funcs.o thumb-ops.o walk.o
	$(CC) $^ $(LDFLAGS) -o $@

