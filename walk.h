#ifndef WALK_H
#define WALK_H

struct pair {
	char *path;
	char hashed[33];
};


struct mylist {
	/* number of allocated elements */
	size_t size;
	/* number of real elements in the list */
	size_t current;
	struct pair *data;
};


void list_init(struct mylist *l);

void list_destroy(struct mylist *l);

/* walk in `path` tree and hashes every regular file's URL */
void inode_to_hash(const char *path, struct mylist *store);

#endif /* WALK_H */
