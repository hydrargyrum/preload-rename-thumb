#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>

#include "md5.h"
#include "png.h"
#include "real-funcs.h"
#include "thumb-ops.h"


typedef char*(*realpath_t)(const char*, char*);

static int is_verbose;

static __attribute__((constructor)) void init(void)
{
	const char *verbose_str = getenv("VERBOSE_THUMBNAIL");

	if (verbose_str && strcmp(verbose_str, "1") == 0) {
		is_verbose = 1;
	}
}

static char *better_realpath(const char *path)
{
	char *full = NULL;
	char *tmp = NULL;
	char *path2 = strdup(path);

	full = canonicalize_file_name(path);
	if (full == NULL) {
		if (errno == ENOENT) {
			char *par = dirname(path2);

			tmp = canonicalize_file_name(par);
			if (tmp == NULL) {
				goto cleanup;
			}

			strcpy(path2, path);
			asprintf(&full, "%s/%s", tmp, basename(path2));
		} else {
			goto cleanup;
		}
	}

cleanup:
	free(path2);
	free(tmp);
	return full;
}


#define URL_PREFIX "file://"

/* canonicalizes file path and make it a file:// URL */
static char *create_file_url(const char *path)
{
	char *full = NULL;
	char *url = NULL;

	full = better_realpath(path);
	if (full == NULL) {
		goto cleanup;
	}
	url = malloc(strlen(full) + strlen(URL_PREFIX) + 1);
	if (url == NULL) {
		goto cleanup;
	}
	url[0] = 0;
	strcat(strcat(url, URL_PREFIX), full);

cleanup:
	free(full);
	return url;
}


#define DEFAULT_THUMB_PATH ".cache/thumbnails"

/* general notes in this file:
 * - `size` is often a string containing "normal" or "large" as per XDG Thumbnail Managing
 *   Standard
 * - `digest` is a hex-digits string containing the MD5 checksum of file URL
 */

static char *get_thumb_path(const char *size, const char *digest, const char *extension)
{
	/* TODO use XDG_CACHE_HOME if set */
	const char *home = getenv("HOME");
	char *path = NULL;

	if (home == NULL) {
		return NULL;
	}
	asprintf(&path, "%s/%s/%s/%s.%s", home, DEFAULT_THUMB_PATH, size, digest, extension);
	return path;
}


#define CHECK(expr)			\
	do {				\
		if ((expr) == NULL) {	\
			goto cleanup;	\
		}			\
	} while (0)

static void rename_thumb(
	const char *url_new,
	const char hash_old[33], const char hash_new[33],
	const char *size
)
{
	char *thumb_old = NULL;
	char *thumb_new = NULL;

	CHECK(thumb_old = get_thumb_path(size, hash_old, "png"));
	CHECK(thumb_new = get_thumb_path(size, hash_new, "png"));
	if (real_rename(thumb_old, thumb_new) == 0) {
		if (update_metadata(thumb_new, url_new) == 0 && is_verbose) {
			fprintf(stderr, "renamed '%s' to '%s'\n", thumb_old, thumb_new);
		}
	}

cleanup:
	free(thumb_old);
	free(thumb_new);
}

void rename_thumbs(const char *oldpath, const char *newpath)
{
	char *url_old = NULL;
	char *url_new = NULL;
	char hash_old[33];
	char hash_new[33];

	CHECK(url_old = create_file_url(oldpath));
	CHECK(url_new = create_file_url(newpath));

	compute_md5(url_old, hash_old);
	compute_md5(url_new, hash_new);

	rename_thumb(url_new, hash_old, hash_new, "large");
	rename_thumb(url_new, hash_old, hash_new, "normal");

cleanup:
	free(url_new);
	free(url_old);
}

void rename_thumbs_dir(const char *newprefix, const struct mylist *treehashes)
{
	char newpath[PATH_MAX];
	char *prefixend;

	strcpy(newpath, newprefix);
	prefixend = newpath + strlen(newpath);

	for (size_t current = 0; current < treehashes->current; current++) {
		struct pair *curpair = &treehashes->data[current];
		char *url_new;
		char hash_new[33];

		if (prefixend + 2 + strlen(curpair->path) - newpath >= PATH_MAX) {
			/* size isn't enough */
			continue;
		}
		*prefixend = 0;
		/* curpair->path has a leading slash.
		 * since the whole dir was moved, the newpath must exist and
		 * its file content can't have changed.
		 */
		strcat(prefixend, curpair->path);

		url_new = create_file_url(newpath);
		compute_md5(url_new, hash_new);

		rename_thumb(url_new, curpair->hashed, hash_new, "large");
		rename_thumb(url_new, curpair->hashed, hash_new, "normal");

		free(url_new);
	}
}

static void unlink_thumb(const char hash[33], const char *size)
{
	char *thumb = NULL;

	CHECK(thumb = get_thumb_path(size, hash, "png"));
	if (real_unlink(thumb) == 0 && is_verbose) {
		fprintf(stderr, "removed '%s'\n", thumb);
	}

cleanup:
	free(thumb);
}

void unlink_thumbs(const char *path)
{
	char *url = NULL;
	char hash[33];

	CHECK(url = create_file_url(path));

	compute_md5(url, hash);

	unlink_thumb(hash, "large");
	unlink_thumb(hash, "normal");

cleanup:
	free(url);
}
