#include <wand/MagickWand.h>


#define WAND_CHECK(v)				\
	do {					\
		if ((v) == MagickFalse) {	\
			goto cleanup;		\
		}				\
	} while (0)


int update_metadata(const char *thumb, const char *uri)
{
	MagickWand *wand = NULL;
	MagickBooleanType status;

	MagickWandGenesis();

	wand = NewMagickWand();
	WAND_CHECK(status = MagickReadImage(wand, thumb));
	WAND_CHECK(status = MagickSetImageProperty(wand, "Thumb::URI", uri));
	WAND_CHECK(status = MagickWriteImage(wand, NULL));

cleanup:
	if (wand) {
		DestroyMagickWand(wand);
	}
	return (status == MagickFalse) ? 1 : 0;
}
