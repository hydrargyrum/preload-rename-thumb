
/* hashes string `in` into `out`.
 * `out` will be hex-digits plus terminating null char.
 */
void compute_md5(const char *in, char out[33]);
