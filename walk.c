
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <assert.h>

#include "md5.h"
#include "walk.h"

void list_init(struct mylist *l)
{
	memset(l, 0, sizeof(struct mylist));
}

void list_destroy(struct mylist *l)
{
	for (size_t current = 0; current < l->current; current++) {
		free(l->data[current].path);
	}
	free(l->data);
	memset(l, 0, sizeof(struct mylist));
}

static void list_append(struct mylist *l, struct pair p)
{
	if (l->current == l->size) {
		if (l->size) {
			l->size *= 2;
		} else {
			l->size = 2;
		}

		l->data = reallocarray(l->data, l->size, sizeof(struct pair));
	}
	l->data[l->current++] = p;
}

/*
 * path_builder: the absolute path of current dir/subdir (no trailing slash)
 * path_builder_pos: pointer in path_builder to the end of the string,
 *      where to append children paths, for current path
 * path_rel_start: pointer in path_builder to the start of the relative part
 *      to the source dir. starts with a slash. useful for translating to
 *      the destination dir.
 * url_builder: absolute url of current dir/subdir (no trailing slash)
 * url_builder_pos: pointer in url_builder to the end of the string.
 *
 * example for bar/baz:
 * /foo/bar/baz\0
 * ^ path_builder
 *             ^ path_builder_pos
 *     ^ path_rel_start
 */
static void walk_tree(
	struct mylist *store,
	char *path_builder, char *path_builder_pos,
	char *path_rel_start,
	char *url_builder, char *url_builder_pos
)
{
	DIR *dir = opendir(path_builder);
	struct pair p;

	for (struct dirent *cur = readdir(dir); cur != NULL; cur = readdir(dir)) {
		size_t namelen;

		if (cur->d_type != DT_DIR && cur->d_type != DT_REG) {
			continue;
		}
		if (strcmp(cur->d_name, ".") == 0 || strcmp(cur->d_name, "..") == 0) {
			continue;
		}
		namelen = strlen(cur->d_name);

		if ((path_builder_pos + 2 + namelen) - path_builder >= PATH_MAX) {
			/* the size wouldn't be enough */
			continue;
		}
		*path_builder_pos = 0;
		strcat(path_builder_pos, "/");
		strcat(path_builder_pos, cur->d_name);

		if ((url_builder_pos + 2 + namelen) - path_builder >= PATH_MAX) {
			continue;
		}
		*url_builder_pos = 0;
		strcat(url_builder_pos, "/");
		strcat(url_builder_pos, cur->d_name);

		if (cur->d_type == DT_DIR) {
			/* the depth-first traversal makes sure path_builder
			   stays with same prefix
			*/
			walk_tree(
				store,
				path_builder, path_builder_pos + 1 + namelen,
				path_rel_start,
				url_builder, url_builder_pos + 1 + namelen
			);
		} else if (cur->d_type == DT_REG) {
			compute_md5(url_builder, p.hashed);
			p.path = strdup(path_rel_start);

			list_append(store, p);
		}
	}

	closedir(dir);
}

void inode_to_hash(const char *path, struct mylist *store)
{
	char path_builder[PATH_MAX];
	char url_builder[PATH_MAX] = "file://";
	char *prefix = canonicalize_file_name(path);
	
	if (prefix == NULL) {
		goto end;
	}

	strcpy(path_builder, prefix);
	strcat(url_builder, prefix);
	walk_tree(
		store,
		path_builder, path_builder + strlen(path_builder),
		path_builder + strlen(path_builder),
		url_builder, url_builder + strlen(url_builder)
	);

end:
	free(prefix);
	return;
}
