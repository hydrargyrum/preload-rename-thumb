
int real_rename(const char *oldpath, const char *newpath);

int real_renameat2(
	int olddirfd, const char *oldpath,
	int newdirfd, const char *newpath, unsigned int flags
);

int real_unlink(const char *pathname);

int real_unlinkat(int dirfd, const char *pathname, int flags);
