#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "md5.h"
#include "png.h"
#include "real-funcs.h"
#include "thumb-ops.h"
#include "walk.h"


int rename(const char *oldpath, const char *newpath)
{
	int result;
	int old_errno;
	struct stat statbuf;
	struct mylist treehashes;

	list_init(&treehashes);

	result = lstat(oldpath, &statbuf);
	if (result != 0) {
		statbuf.st_mode = 0;
	}
	if (statbuf.st_mode & S_IFDIR) {
		inode_to_hash(oldpath, &treehashes);
	}

	result = real_rename(oldpath, newpath);
	old_errno = errno;

	if (result == 0) {
		if (statbuf.st_mode & S_IFDIR) {
			rename_thumbs_dir(newpath, &treehashes);
		} else if (statbuf.st_mode & S_IFREG) {
			rename_thumbs(oldpath, newpath);
		}
	}

	list_destroy(&treehashes);
	errno = old_errno;
	return result;
}

int renameat2(
	int olddirfd, const char *oldpath,
	int newdirfd, const char *newpath, unsigned int flags
)
{
	int result;
	int old_errno;
	struct stat statbuf;
	struct mylist treehashes;

	list_init(&treehashes);

	result = lstat(oldpath, &statbuf);
	if (result != 0) {
		/* nevermind the thumbnails, let's do the real rename at least */
		statbuf.st_mode = 0;
	}
	if (statbuf.st_mode & S_IFDIR) {
		/* TODO try to detect some conditions where the rename will likely fail
		 * and skip this costly tree walk then
		 */
		inode_to_hash(oldpath, &treehashes);
	}

	result = real_renameat2(olddirfd, oldpath, newdirfd, newpath, flags);
	old_errno = errno;

	if (
		result == 0
		&& olddirfd == AT_FDCWD && newdirfd == AT_FDCWD
		&& !(flags & RENAME_EXCHANGE)
	) {
		/* no need to rename thumbs if real_rename failed.
		 * TODO implement for dirfd or "exchange"
		 */
		if (statbuf.st_mode & S_IFDIR) {
			rename_thumbs_dir(newpath, &treehashes);
		} else if (statbuf.st_mode & S_IFREG) {
			rename_thumbs(oldpath, newpath);
		}
	}

	list_destroy(&treehashes);
	errno = old_errno;
	return result;
}

int renameat(
	int olddirfd, const char *oldpath,
	int newdirfd, const char *newpath
)
{
	/* renameat is a different syscall, maybe we should wrap like we did
	 * for renameat2. But does it make a real difference if we implement it
	 * by merely reusing renameat2?
	 */
	return renameat2(olddirfd, oldpath, newdirfd, newpath, 0);
}

int unlink(const char *pathname)
{
	int result;
	int old_errno;

	result = real_unlink(pathname);
	old_errno = errno;

	if (result == 0) {
		unlink_thumbs(pathname);
	}

	errno = old_errno;
	return result;
}

int unlinkat(int dirfd, const char *pathname, int flags)
{
	int result;
	int old_errno;

	result = real_unlinkat(dirfd, pathname, flags);
	old_errno = errno;

	if (result == 0 && dirfd == AT_FDCWD) {
		unlink_thumbs(pathname);
	}

	errno = old_errno;
	return result;
}
