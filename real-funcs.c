#include <dlfcn.h>

#define WRAP_DECL(rtype, name, ...)				\
	typedef rtype (*real_ ## name ## _t)(__VA_ARGS__);	\
	rtype real_ ## name (__VA_ARGS__)

#define WRAP_BODY(name, ...)	\
	do {								\
		real_ ## name ## _t func = dlsym(RTLD_NEXT, #name);	\
		return func(__VA_ARGS__);				\
	} while (0)

WRAP_DECL(int, rename, const char *oldpath, const char *newpath)
{
	WRAP_BODY(rename, oldpath, newpath);
}

WRAP_DECL(
	int, renameat2, int olddirfd, const char *oldpath,
	int newdirfd, const char *newpath, unsigned int flags
)
{
	WRAP_BODY(renameat2, olddirfd, oldpath, newdirfd, newpath, flags);
}

WRAP_DECL(int, unlink, const char *pathname)
{
	WRAP_BODY(unlink, pathname);
}

WRAP_DECL(int, unlinkat, int dirfd, const char *pathname, int flags)
{
	WRAP_BODY(unlinkat, dirfd, pathname, flags);
}
