#!/usr/bin/env python3

import sys

import vignette


print(vignette.try_get_thumbnail(sys.argv[1]))
